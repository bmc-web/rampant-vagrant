#!/usr/bin/env bash
#drupalInstall.sh
#
#Drupal provisioning Script


#Download and extract Drupal 7.x
    echo "Beginning Drupal Provisioning"
    chmod 750 -R /vagrant/public_html
    rm -rf /vagrant/public_html/
    mkdir /vagrant/public_html
    cd /vagrant/public_html
	drush dl drupal-7.x
	mv /vagrant/public_html/drupal-7.x-dev/* /vagrant/public_html
	mv /vagrant/public_html/drupal-7.x-dev/.htaccess /vagrant/public_html
	mv /vagrant/public_html/drupal-7.x-dev/.gitignore /vagrant/public_html
	rm -R /vagrant/public_html/drupal-7.x-dev
	drush si --db-url=mysql://root:root@localhost/Web --account-name=vagrant --account-pass=vagrant --clean-url --site-name=website.vbox.local -y
	mkdir /vagrant/public_html/sites/all/modules/contrib
	mkdir /vagrant/public_html/sites/all/modules/custom
	cd /vagrant/public_html
	drush en module_filter -y
	drush en adminimal_admin_menu -y
	drush dis overlay -y
	drush up drupal -y

echo "Drupal installation ready! Login to http://website.vbox.local with vagrant:vagrant"