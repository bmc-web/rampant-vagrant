#!/usr/bin/env bash
# cleanGrav.sh
#
# Installs and configures GravCMS

# Clean up public_html directory
    echo "Cleaning up public_html directory..."
    cd /vagrant/public_html
    rm -rf /vagrant/public_html
    mkdir -p /vagrant/public_html
    cd /tmp
    cd /vagrant/public_html

# Check PHP version, upgrade if necessary
    VER=$(php -i | grep "Module version" | awk '{print $4}')
    MVER=$(echo $VER | cut -c1)
    if [ $MVER -ne 7 ]
        then
          echo "Installed PHP version ($VER) is too old."
          chmod +x /vagrant/bootstrapScripts/CentOS/upgradeLAMP.sh
          sh /vagrant/bootstrapScripts/CentOS/upgradeLAMP.sh
    fi

# Install GravCMS
    echo "Installing GravCMS and plugins"
    cd /vagrant/public_html
    composer create-project getgrav/grav .
    bin/gpm index --force
    bin/gpm install admin devtools admin-addon-user-manager -y

# Setup Accounts
    bin/plugin login newuser -u vagrant -e vagrant@website.vbox.local -p vagrantAdmin1 -P b -N "Vagrant Admin"

# Create initial backup starting point
    bin/gpm self-upgrade -y
    bin/gpm update -y
    bin/grav clear-cache
    bin/grav backup

echo "GravCMS is installed.  Login with vagrant:vagrantUser1 at http://website.vbox.local/admin"