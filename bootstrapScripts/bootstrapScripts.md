#bootstrapScripts.md
The scripts below are used in various combinations to auto-build test environments within the vagrant box.  Add the appropriate entries within the vagrant.local.yml to add the desired functionality.  The vagrant.local.yml.example file demonstrates the structure.
## CentOS
### CentOS_LAMP.sh
* Default part of vagrant build sequence.
* Provisions basic LAMP installation and configuration for CentOS.
### netbeans81.sh
* Installs and configures Netbeans locally in the vagrant box.
### upgradeLAMP.sh
* Upgrades PHP to php7 and MySQL to v5.5
## Webapp
### cleanDrupal7Install.sh
* Downloads and installs latest stable Drupal 7.x core
* Default user:pass is vagrant:vagrant
### cleanDrupal8Install.sh
* Downloads and installs latest stable Drupal 8.x core
* Default user:pass is vagrant:vagrant
### cleanGravInstall.sh
* Downloads and installs latest stable Grav core
* Default user:pass is vagrant:vagrantAdmin1
### cleanWordpressInstall.sh
* Downloads and installs latest stable Wordpress core
* Default user:pass is vagrant:vagrant
### developerSuite.sh
* Default part of vagrant build sequence.
* Downloads and installs various toolsets (Code Sniffer, Behat, Selenium, Drush, wp-cli, etc.) 
### tarDrupalLoad.sh
* Performs a drush archive restore from the latest tar file found withing the import folder
* The restore will overwrite all files within the public_html directory of the project
* No default username:password set.  Whatever Drupal users were in the previous environment will be present
## MySQL
### sqlLoad.sh
* Drops local "Web" MySQL database and imports from latest sql file found in import folder