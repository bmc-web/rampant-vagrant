#!/usr/bin/env bash
# CentOS_LAMP.sh
#
#LAMP provisioning script

	# Configure OS
    cd /tmp
    wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
    rpm -ivh epel-release-6-8.noarch.rpm
    yum update -y
    yum install -y lynx php-soap php-ldap php-mbstring

  # Install Ruby (taken from http://tecadmin.net/install-ruby-1-9-3-or-multiple-ruby-verson-on-centos-6-3-using-rvm)
    yum -y install gcc-c++ patch readline readline-devel zlib zlib-devel
    yum -y install libyaml-devel libffi-devel openssl-devel make
    yum -y install bzip2 autoconf automake libtool bison iconv-devel
    command curl -sSL https://rvm.io/mpapis.asc | gpg2 --import -
    curl -L get.rvm.io | bash -s stable
    sed -i 's/rvm:x:501:/rvm:x:501:vagrant/' /etc/group
    source /etc/profile.d/rvm.sh
    rvm install 2.4.1
    rvm use 2.4.1 --default
    gem update --system
    gem install bundler

  # Fix Centos 6 root user PATH bug: https://bugs.centos.org/view.php?id=5707
    printf 'export PATH="/usr/local/bin:$PATH"' >> /root/.bashrc

	# Configure Apache
	  echo "Creating symlinks"
		  rm -R /var/www/html
		  ln -fs /vagrant/public_html/ /var/www/html
		  ln -fs /vagrant/public_html/ /home/vagrant/www

		echo "Configuring Apache"
			sed -i 's/apc.shm_size=64M/apc.shm_size=256M/' /etc/php.d/apc.ini
			sed -i 's/#EnableSendfile off/EnableSendfile off/' /etc/httpd/conf/httpd.conf
			sed -i 's/memory_limit = 128M/memory_limit = 256M/' /etc/php.ini
			sed -i 's/post_max_size = 8M/post_max_size = 15M/' /etc/php.ini
			sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 15M/' /etc/php.ini
			sed -i 's/;date.timezone =/date.timezone = America\/New_York/' /etc/php.ini
			sed -i 's/vagrant:x:500:/vagrant:x:500:apache/' /etc/group
			sed -i 's/User apache/User vagrant/' /etc/httpd/conf/httpd.conf
			sed -i 's/Group apache/Group vagrant/' /etc/httpd/conf/httpd.conf
			chown -R vagrant:vagrant /var/lib/php/session
			printf 'start on vagrant-mounted\nexec sudo service httpd start' > /etc/init/vagrant-mounted.conf
      printf '<VirtualHost *:80>\n\tAlias /codiad "/vagrant/apps/codiad"\n\tDocumentRoot /var/www/html\n\tServerName website.vbox.local\n\t<Directory "/var/www/html">\n\t\tAllowOverride All\n\t</Directory>\n\t<Directory "/vagrant/apps/codiad">\n\t\tAllowOverride All\n\t</Directory>\n</VirtualHost>\n' >> /etc/httpd/conf/httpd.conf
      service httpd restart

	# Configure MySQL Server
		echo "Installing MySQL"
			yum -y install mysql mysql-server mytop
			service mysqld restart
			/usr/bin/mysqladmin -u root password 'root'
			mysql -u root -e 'create database Web' -proot
			sed '/^symbolic-links/a max_allowed_packet=512M' /etc/my.cnf > /etc/my2.cnf
			rm /etc/my.cnf
			mv /etc/my2.cnf /etc/my.cnf
			printf '[client]\n\tuser=root\n\thost=localhost\n\tpassword=root\n' > /home/vagrant/.my.cnf
			chown vagrant:vagrant /home/vagrant/.my.cnf

	# Setup startup applications
		chkconfig mysqld on
		chkconfig httpd on

	# Restart services
		service mysqld restart
		service httpd restart

	echo "CentOS completed provisioning"