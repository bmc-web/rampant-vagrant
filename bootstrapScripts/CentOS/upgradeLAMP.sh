#!/usr/bin/env bash
# upgradeLamp.sh
#
# Upgrade instructions for PHP and MySQL from https://webtatic.com/packages/php70/

# Upgrade PHP (php7)
    echo "Upgrading LAMP to v7"
    cd /tmp
    rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
    rpm -Uvh https://mirror.webtatic.com/yum/el6/latest.rpm
    yum install -y yum-plugin-replace
    yum replace -y php-common --replace-with=php70w-common
    yum install -y php70w php70w-opcache

# Upgrade mysql (mysql5.5)
    echo "Upgrading MySQl to v5.5"
    yum install -y mysql.`uname -i` yum-plugin-replace
    yum replace -y mysql --replace-with mysql55w
    yum erase -y php-mysql
    yum install -y php-mysqlnd

# Fix permissions for Linux hosts
    echo "Changing permissions so Linux/ Mac hosts can serve content. Windows users are unaffected."
    sed -i 's/User apache/User vagrant/' /etc/httpd/conf/httpd.conf
		sed -i 's/Group apache/Group vagrant/' /etc/httpd/conf/httpd.conf
		chown -R vagrant:vagrant /var/lib/php/session

# Restart services
    echo "Restarting Services"
    service httpd restart
    service mysqld restart
    chkconfig httpd on
    chkconfig mysqld on
